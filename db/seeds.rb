# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

10.times do |user|
  u = User.create(email: Faker::Internet.email, password: "changeme", password_confirmation: "changeme")
  100.times do |product|
    u.products.create(name: Faker::ProgrammingLanguage.name, price: Faker::Number.decimal(3,2))
  end
end
