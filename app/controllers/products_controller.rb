class ProductsController < ApplicationController
  # before_action :authenticate_request!

  def index
    render json: {products: Product.limit(3).to_json}
  end
end