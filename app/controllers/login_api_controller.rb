class LoginApiController < ApplicationController
  before_action :authenticate_request!

  def fetch_products
    render json: {message: 'logged in', products: current_user.products.to_json}
  end
end