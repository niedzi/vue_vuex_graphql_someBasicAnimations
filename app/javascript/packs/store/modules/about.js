import * as types from '../mutation-types'

const state = {
  count: 0,
}

const getters = {}

const mutations = {
  [types.INCREMENT] (state){ state.count++ },
  [types.DECREMENT] (state) { state.count-- }
}

const actions = {}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}