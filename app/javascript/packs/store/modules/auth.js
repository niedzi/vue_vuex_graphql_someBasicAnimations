import * as types from '../mutation-types'
import axios from "axios";

const state = {
  token: ''
}

const getters = {
  isLogged: state => {
    return state.token.length > 0
  },
  getToken: state => {
    return state.token
  }
}

const mutations = {
  [types.SET_TOKEN] (state, payload){ state.token = payload },
  [types.CLEAR_TOKEN] (state) { state.token = '' }
}

const actions = {
  setToken ({ commit }, {email, password}){
    axios.post('http://localhost:3000/auth_user', {email: email, password: password})
      .then(response => {
        commit(types.SET_TOKEN, response.data.auth_token)
        console.clear()
      })
      .catch(error => console.log(error))
  },
  clearToken ({ commit }){
    commit(types.CLEAR_TOKEN, '')
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}