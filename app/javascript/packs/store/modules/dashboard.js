import * as types from '../mutation-types'
import axios from "axios/index";

const state = {
  count: 0,
  weather: {}
}

const getters = {
  odd: state => {
    return state.count%2==1
  },
  getCoordLon: state => {
    return Object.keys(state.weather).length > 0 ? state.weather.coord.lon : ''
  },
  getCoordLat: state => {
    return Object.keys(state.weather).length > 0 ? state.weather.coord.lat : ''
  }
}


const mutations = {
  [types.INCREMENT] (state){ state.count++ },
  [types.DECREMENT] (state) { state.count-- },
  [types.SET_WEATHER] (state, payload) { state.weather = payload }
}

const actions = {
  incrementAction ({ commit }){
    setTimeout(() => {
      commit(types.INCREMENT)
    }, 2000)
  },
  getWeather ({ commit }){
    axios.get("https://api.openweathermap.org/data/2.5/weather?q=Torun,pl&APPID=0644b27d03c7416987cf03f9d0bf522f")
      .then(response => commit(types.SET_WEATHER, response.data))
      .catch(error => console.log(error))
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}