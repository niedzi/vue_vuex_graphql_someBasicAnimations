import Vue from 'vue'
import Vuex from 'vuex'

import dashboard from './modules/dashboard'
import about from './modules/about'
import auth from './modules/auth'
import createLogger from 'vuex/dist/logger'

import { first_plugin } from './plugins/first_plugin'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    dashboard,
    about,
    auth
  },
  plugins: [first_plugin, createLogger()],
  // strict: true only on devel
})