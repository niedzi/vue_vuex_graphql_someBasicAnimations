import axios from 'axios'


export default{
  data: function (){
    return {
      weather: {}
    }
  },
  methods: {
    getWeatherMixin: function(){
      axios.get('https://api.openweathermap.org/data/2.5/weather?q=Torun,pl&APPID=0644b27d03c7416987cf03f9d0bf522f')
        .then(response => this.weather = response.data)
        .catch(error => console.log(error))
    }
  }
}