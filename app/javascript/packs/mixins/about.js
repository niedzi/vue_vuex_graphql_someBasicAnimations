import { mapMutations } from 'vuex'

export default{
  computed: {
    count: function () {
      return this.$store.state.about.count
    }
  },
  methods: {
    ...mapMutations('about', ['increment', 'decrement'])
  }
}