import { mapMutations, mapActions, mapGetters } from 'vuex'

export default{
  computed: {
    count: function () {
      return this.$store.state.dashboard.count
    },
    api_key: function () {
      return this.$store.state.dashboard.api_key
    },
    currentWeather: function () {
      return this.$store.state.dashboard.weather
    }
  },
  methods: {
    ...mapGetters('dashboard', {odd: 'odd', getCoordLon: 'getCoordLon', getCoordLat: 'getCoordLat'}),
    ...mapMutations('dashboard', ['increment', 'decrement', 'setWeather']),
    ...mapActions('dashboard', {incrementAction: 'incrementAction', getWeather: 'getWeather'})
  }
}