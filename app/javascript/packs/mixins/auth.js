import { mapMutations, mapActions, mapGetters } from 'vuex'

export default{
  // if you will map mutations and action with same name action/mutation will be done twice or n times so action will be doubled
  methods: {
    ...mapGetters('auth', ['isLogged', 'getToken']),
    ...mapMutations('auth', ['setToken', 'clearToken']),
    ...mapActions('auth', {logIn: 'setToken', logOut: 'clearToken'})
  }
}