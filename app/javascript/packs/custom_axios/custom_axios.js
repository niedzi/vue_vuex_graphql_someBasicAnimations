import axios from 'axios'

var instance = axios.create()
var CSRV = document.querySelectorAll('[name="csrf-token"]')[0].content
instance.defaults.headers.common['X-CSRF-Token'] = CSRV

export default instance