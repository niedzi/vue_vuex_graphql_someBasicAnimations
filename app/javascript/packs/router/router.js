import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Dashboard from '../components/dashboard'
import About from '../components/about'
import Products from '../components/products'
import Login from '../components/login'

export default new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { path: '/', component: Dashboard },
    { path: '/about', component: About },
    { path: '/products', component: Products },
    { path: '/login', component: Login }
  ]
})