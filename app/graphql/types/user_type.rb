Types::UserType = GraphQL::ObjectType.define do
  name 'User'

  field :id, !types.ID
  field :email, !types.String
  field :products, !types[Types::ProductType] do
    argument :limit, types.Int, default_value: 20, prepare: -> (limit) { [limit, 30].min }
    argument :id, types.Int
    resolve ->(obj, args, ctx) {
      # puts "obj"
      # puts obj.inspect
      # puts "args"
      # puts args.inspect
      # puts "ctx"
      # puts ctx.inspect
      # puts "-------"
      if args[:id].present?
        obj.products.where(id: args[:id]).limit(args[:limit])
      else
        obj.products.limit(args[:limit])
      end
    }
  end
end