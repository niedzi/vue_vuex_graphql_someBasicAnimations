Types::QueryType = GraphQL::ObjectType.define do
  name "Query"
  # Add root-level fields here.
  # They will be entry points for queries on your schema.
  #
  # here we enter QUERY

  field :users, !types[Types::UserType] do
    description "Listing all users"
    argument :limit, types.Int, default_value: 20, prepare: -> (limit) { [limit, 200].min }
    argument :id, types.Int
    resolve ->(obj, args, ctx) {
      # puts "obj"
      # puts obj.inspect
      # puts "args"
      # puts args.inspect
      # puts "ctx"
      # puts ctx.inspect
      # puts "-------"
      if args[:id].present?
        User.where(id: args[:id]).limit(args[:limit])
      else
        User.limit(args[:limit])
      end
    }
  end

  field :products, !types[Types::ProductType] do
    description "Listing all products"
    argument :limit, types.Int, default_value: 20, prepare: -> (limit) { [limit, 200].min }
    argument :id, types.Int
    resolve ->(obj, args, ctx) {
      # puts "obj"
      # puts obj.inspect
      # puts "args"
      # puts args.inspect
      # puts "ctx"
      # puts ctx.inspect
      # puts "-------"
      if args[:id].present?
        Product.where(id: args[:id]).limit(args[:limit])
      else
        Product.limit(args[:limit])
      end
    }
  end

end
