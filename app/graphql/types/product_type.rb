Types::ProductType = GraphQL::ObjectType.define do
  name 'Product'

  field :id, !types.ID
  field :name, !types.String
  field :price, !types.Float
  field :user, Types::UserType do
    resolve ->(obj, args, ctx) {
      # puts "obj"
      # puts obj.inspect
      # puts "args"
      # puts args.inspect
      # puts "ctx"
      # puts ctx.inspect
      # puts "-------"
      obj.user
    }
  end
end