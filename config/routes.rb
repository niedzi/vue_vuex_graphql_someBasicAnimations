Rails.application.routes.draw do
  devise_for :users
  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
  end

  post "/graphql", to: "graphql#execute"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "home#index"
  get 'about' => "home#index"
  get 'products' => "home#index"
  get 'login' => "home#index"
  get 'fetch_products' => "login_api#fetch_products"
  get 'products_with_csrv' => "products#index", defaults: {format: :json}
  post 'auth_user' => 'authentication#authenticate_user'

end
